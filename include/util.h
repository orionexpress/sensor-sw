/*
 * util.h
 *
 *  Created on: 2015 dec. 11
 *      Author: palotasb
 */

#ifndef UTIL_H_
#define UTIL_H_

#include "stm32f0xx.h"
#include "tim14_periph.h"

typedef struct
{
    uint32_t StartTime;
    uint32_t Tag;
    uint32_t MaxTime;
    uint32_t MaxTag;
    uint32_t MaxStartTime;
} TM;

void TMStart(TM*, uint32_t);
uint32_t TMEnd(TM*);

void quick_sort_8(int8_t *a, uint32_t n);
void quick_sort_u32(uint32_t *a, uint32_t n);
void CopyData(void* src, void* dst, unsigned int size);

#endif /* UTIL_H_ */
