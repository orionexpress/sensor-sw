/*
 * usart1_periph.h
 *
 *  Created on: 2015 nov. 30
 *      Author: palotasb
 */

#ifndef USART1_PERIPH_H_
#define USART1_PERIPH_H_

#include "stm32f0xx.h"
#include "fifo.h"
#include "criticalsection.h"
#include "dma1_periph.h"

struct USART1_MsgType;

#define USART1_DATALENGTH   8
#define USART1_TXMSG_SIZE   (USART1_DATALENGTH + 1)
#define USART1_RXMSG_SIZE   (USART1_DATALENGTH + 1)

typedef enum
{
    USART1_TxStatus_Reset = 0,
    USART1_TxStatus_Sending,
    USART1_TxStatus_Ready
} USART1_TxStatusTypeDef;

typedef struct USART1_MsgType
{
    uint8_t Length;
    uint8_t Data[USART1_DATALENGTH];
} USART1_MsgTypeDef;

extern USART1_TxStatusTypeDef USART1_TxStatus;
extern uint8_t USART1_TxData[USART1_TXMSG_SIZE];
extern uint8_t USART1_RxData[USART1_RXMSG_SIZE];

void USART1_Init(void);
ErrorStatus USART1_SendData(uint8_t* data, uint8_t length);
ErrorStatus USART1_QueueMsg(USART1_MsgTypeDef* msgPtr);

#endif /* USART1_PERIPH_H_ */
