/*
 * linetrack_modules.c
 *
 *  Created on: 2015 nov. 16
 *      Author: palotasb
 */

#include "linetrack_modules.h"

// When placed inside xxx_SensorCalibrationLowValues, then the given sensor will always give back 0
// if UseSensorCalibrationValues is ENABLE at the same time in the same LINETRACK-module
#define SENSOR_IGNORE   0xFFFF

LINETRACK_ModuleTypeDef LTM1;
uint32_t LTM1_SensorData[LTM1_SENSORCOUNT];
uint32_t LTM1_TrackingPositions[LTM_LINECOUNT];
uint32_t LTM1_FilteredTrackingPositions[LTM_LINECOUNT];
uint32_t LTM1_SensorDataTmp[LTM1_SENSORCOUNT];
uint8_t LTM1_SensorDataAboveDetectionThreshold[LTM1_SENSORCOUNT];
uint32_t const LTM1_SensorCalibrationLowValues[LTM1_SENSORCOUNT] =
{ SENSOR_IGNORE, SENSOR_IGNORE, 4608, 3584, 11264, 4352, 16025, 31744, 3328, 3328, 3328, 3072, 3328, 3328, 3123, 3072,
  3584, 3328, 3328, 3328, 3225, 3584, 3328, 3276, 21504, 4300, 3840, 3020, 3072, 3072, SENSOR_IGNORE, SENSOR_IGNORE };
uint32_t const LTM1_SensorCalibrationHighValues[LTM1_SENSORCOUNT] =
{ 57384, 56670, 57950, 57789, 58018, 58516, 57681, 60874, 55646, 54582, 55714, 54312, 55121, 55512, 55283, 56374, 57357,
  55714, 54488, 54946, 55458, 55121, 55727, 55970, 58974, 57155, 56226, 56347, 55053, 56158, 54973, 56145 };

LINETRACK_ModuleTypeDef LTM2;
uint32_t LTM2_SensorData[LTM2_SENSORCOUNT];
uint32_t LTM2_TrackingPositions[LTM_LINECOUNT];
uint32_t LTM2_FilteredTrackingPositions[LTM_LINECOUNT];
uint32_t LTM2_SensorDataTmp[LTM2_SENSORCOUNT];
uint8_t LTM2_SensorDataAboveDetectionThreshold[LTM2_SENSORCOUNT];
uint32_t const LTM2_SensorCalibrationLowValues[LTM2_SENSORCOUNT] =
{ SENSOR_IGNORE, SENSOR_IGNORE, 3174, 3328, 3072, 3328, 3584, 14540, 2816, 2969, 3072, 3328, 3020, 3072, 3072, 3072,
  3328, 3072, 3072, 3328, 2867, 3072, 3072, 3072, 12083, 3072, 3072, 3072, 3737, 3020, SENSOR_IGNORE, SENSOR_IGNORE };
uint32_t const LTM2_SensorCalibrationHighValues[LTM2_SENSORCOUNT] =
{ 50048, 52710, 53338, 52979, 53389, 53581, 52634, 55782, 47846, 49216, 50074, 50714, 49178, 49920, 50022, 49216, 51750,
  51558, 52173, 53094, 50765, 52634, 50957, 51661, 56205, 53798, 52083, 52429, 52506, 49869, 51840, 51635 };

static TM TMCalc;

static void LTM_SendResults(void);

void LTM_Init(void)
{
    // 1/4 Init Sensing module
    SENSING_SensorData1 = LTM1_SensorData;
    SENSING_SensorData2 = LTM2_SensorData;

    SENSING_Init();

    // 2/4 Init Linetrack module 1
    LTM1.TrackingPositions = LTM1_TrackingPositions;
    LTM1.FilteredTrackingPositions = LTM1_FilteredTrackingPositions;
    LTM1.TrackingPositionCapacity = LTM_LINECOUNT;

    LTM1.TrackWidth = 5;

    LTM1.SensorData = LTM1_SensorData;
    LTM1.SensorDataTmp = LTM1_SensorDataTmp;
    LTM1.SensorDataCount = 32;
    LTM1.SensorDataAboveDetectionThreshold = LTM1_SensorDataAboveDetectionThreshold;

    LTM1.PreDetectionMedianFilter = ENABLE;
    LTM1.PreDetectionFIRFilter = DISABLE;
    LTM1.DetectionThreshold = 0x00008000;

    LTM1.FIRFilter = DISABLE;

    LTM1.UseSensorCalibration = ENABLE;
    LTM1.SensorCalibrationLowValues = (uf24p8*) LTM1_SensorCalibrationLowValues;
    LTM1.SensorCalibrationHighValues = (uf24p8*) LTM1_SensorCalibrationHighValues;

    LINETRACK_Init(&LTM1);

    // 3/4 Init Linetrack module 2

    LTM2.TrackingPositions = LTM2_TrackingPositions;
    LTM2.FilteredTrackingPositions = LTM2_FilteredTrackingPositions;
    LTM2.TrackingPositionCapacity = LTM_LINECOUNT;

    LTM2.TrackWidth = 5;

    LTM2.SensorData = LTM2_SensorData;
    LTM2.SensorDataTmp = LTM2_SensorDataTmp;
    LTM2.SensorDataCount = 32;
    LTM2.SensorDataAboveDetectionThreshold = LTM2_SensorDataAboveDetectionThreshold;

    LTM2.PreDetectionMedianFilter = ENABLE;
    LTM2.PreDetectionFIRFilter = DISABLE;
    LTM2.DetectionThreshold = 0x00008000;

    LTM2.FIRFilter = DISABLE;

    LTM2.UseSensorCalibration = ENABLE;
    LTM2.SensorCalibrationLowValues = (uf24p8*) LTM2_SensorCalibrationLowValues;
    LTM2.SensorCalibrationHighValues = (uf24p8*) LTM2_SensorCalibrationHighValues;

    LINETRACK_Init(&LTM2);

    // 4/4 Init SPI1 module
    SPI1_Init();
}

void LTM_MeasureBaselineThreshold(void)
{
    uint32_t i;
    uf24p8 dts1 = 0, dts2 = 0;

    // detection thresholds and detection threshold sums for average calculation
    for(i = 1; i < 0x10; i++)
    {
        SENSING_FullMeasurement();

        LINETRACK_Calibrate(&LTM1);
        LINETRACK_Calibrate(&LTM2);

        dts1 += LTM1.DetectionThreshold;
        dts2 += LTM2.DetectionThreshold;

        /*
         DEBUG_Write("Calib1; iter");
         DEBUG_WriteU16((uint16_t) i);
         DEBUG_Write(";  ");
         for(j = 0; j < 32; j++)
         {
         DEBUG_WriteU16D((uint16_t)(LTM1.SensorData[j]));
         DEBUG_Write(";");
         if((j % 8) == 7)
         DEBUG_Write("  ");
         }
         DEBUG_Write("\nDetthresh: ");
         DEBUG_WriteU16D(dt1);
         DEBUG_Write("\n");
         //*/
        /*
         DEBUG_Write("Calib2; iter");
         DEBUG_WriteU16((uint16_t) i);
         DEBUG_Write(";  ");
         for(j = 0; j < 32; j++)
         {
         DEBUG_WriteU16D((uint16_t)(LTM2.SensorData[j]));
         DEBUG_Write(";");
         if((j % 8) == 7)
         DEBUG_Write("  ");
         }
         DEBUG_Write("\nDetthresh: ");
         DEBUG_WriteU16D(dt2);
         DEBUG_Write("\n");
         //*/
    }

    dts1 /= i;
    dts2 /= i;
    LTM1.DetectionThreshold = dts1;
    LTM2.DetectionThreshold = dts2;
}

void LTM_Calculate(void)
{
    TMStart(&TMCalc, 0);
    LINETRACK_Calculate(&LTM1);
    LINETRACK_Calculate(&LTM2);
    TMEnd(&TMCalc);
}

void LTM_Start(void)
{
    SENSING_FullMeasurement();
    LTM_Calculate();
    LTM_SendResults();
}

static void LTM_SendResults(void)
{
    uint32_t i;
    int8_t res_frontline[8] =
    { 1, 2, 3, 4, 5, 6, 7, 8 };
    int8_t res_backline[8] =
    { 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27 };
    int8_t tp;

    DEBUG_Leds &= 0x00000000;
    for(i = 0; i < LTM1.TrackingPositionCount && i < 8; i++)
    {
        tp = (int8_t)(LTM1.TrackingPositions[i] >> 5);
        res_frontline[i] = (int8_t)(tp - 127);
        DEBUG_Leds |= (1UL << ((uint8_t) tp >> 3)) & 0xFFFFFFFF;
    }
    if(i == 0)
        DEBUG_Leds |= 0xFFFFFFFF;
    SM_Send(1, (uint8_t) i, (uint8_t*) res_frontline);
    for(i = 0; i < LTM2.TrackingPositionCount && i < 8; i++)
    {
        tp = (int8_t)(LTM2.TrackingPositions[i] >> 5);
        res_backline[i] = (int8_t)(tp - 127);
    }
    SM_Send(2, (uint8_t) i, (uint8_t*) res_backline);
    DEBUG_Update();
}
