/*
 * util.c
 *
 *  Created on: 2015 dec. 11
 *      Author: palotasb
 */

#include "util.h"

void TMStart(TM* tm, uint32_t tag)
{
    tm->StartTime = TIM14_GetCnt();
    tm->Tag = tag;
}

uint32_t TMEnd(TM* tm)
{
    uint16_t delta = (uint16_t) TIM14_GetCnt() - (uint16_t) tm->StartTime;
    volatile uint32_t x;
    if(tm->MaxTime < delta)
    {
        tm->MaxStartTime = tm->StartTime;
        tm->MaxTag = tm->Tag;
        tm->MaxTime = delta;
        if(1000 < delta)
        {
            x = tm->MaxTime;
        }
    }
    return delta;
}

void quick_sort_u32(uint32_t *a, uint32_t n)
{
    uint32_t i, j, p, t;
    if(n < 2)
        return;
    p = a[n / 2];
    for(i = 0, j = n - 1;; i++, j--)
    {
        while(a[i] < p)
            i++;
        while(p < a[j])
            j--;
        if(i >= j)
            break;
        t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
    quick_sort_u32(a, i);
    quick_sort_u32(a + i, n - i);
}

void quick_sort_8(int8_t *a, uint32_t n)
{
    uint32_t i, j;
    int8_t p, t;
    if(n < 2)
        return;
    p = a[n / 2];
    for(i = 0, j = n - 1;; i++, j--)
    {
        while(a[i] < p)
            i++;
        while(p < a[j])
            j--;
        if(i >= j)
            break;
        t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
    quick_sort_8(a, i);
    quick_sort_8(a + i, n - i);
}

void CopyData(void* from, void* to, unsigned int size)
{
    if(((size & 0x03) == 0x00) && ((((unsigned int) from) & 0xFFFFFFFC) == 0)
            && ((((unsigned int) to) & 0xFFFFFFFC) == 0))
    {
        while(size--)
        {
            *(unsigned int*) to = *(unsigned int*) from;
            to = (void*) ((unsigned int) to + sizeof(unsigned int));
            from = (void*) ((unsigned int) from + sizeof(unsigned int));
        }
    }
    else
    {
        while(size--)
        {
            *(unsigned char*) to = *(unsigned char*) from;
            to = (void*) ((unsigned int) to + 1);
            from = (void*) ((unsigned int) from + 1);
        }
    }
}
