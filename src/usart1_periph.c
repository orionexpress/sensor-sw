/*
 * usart1_periph.c
 *
 *  Created on: 2015 nov. 30
 *      Author: palotasb
 */

#include "usart1_periph.h"

#define USART1_GPIO_RCCPeriph   RCC_AHBPeriph_GPIOA
#define USART1_Port             GPIOA

#define USART1_RX_Pin       GPIO_Pin_10
#define USART1_RX_PinSource GPIO_PinSource10
#define USART1_TX_Pin       GPIO_Pin_9
#define USART1_TX_PinSource GPIO_PinSource9
#define USART1_CK_Pin       GPIO_Pin_8
#define USART1_CK_PinSource GPIO_PinSource8

#define USART1_TXFIFO_SIZE  8
#define USART1_BAUDRATE     57600

USART1_TxStatusTypeDef USART1_TxStatus;

uint8_t USART1_TxData[USART1_TXMSG_SIZE];
uint8_t USART1_RxData[USART1_RXMSG_SIZE];

uint8_t USART1_TXFIFOData[USART1_TXMSG_SIZE * USART1_TXFIFO_SIZE];
FIFO USART1_TXFIFO;

static void USART1_TransmitMsg(USART1_MsgTypeDef*);
static void USART1_DMATCCallback(void);

void USART1_Init(void)
{
    USART_InitTypeDef USART1_InitStructure;
    GPIO_InitTypeDef GPIO_USART1_InitStructure;
    USART_ClockInitTypeDef USART1_ClockInitStructure;

    // GPIO AF init
    RCC_AHBPeriphClockCmd(USART1_GPIO_RCCPeriph, ENABLE);
    GPIO_PinAFConfig(USART1_Port, USART1_RX_PinSource, GPIO_AF_1);
    GPIO_PinAFConfig(USART1_Port, USART1_TX_PinSource, GPIO_AF_1);
    GPIO_PinAFConfig(USART1_Port, USART1_CK_PinSource, GPIO_AF_1);

    GPIO_USART1_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_USART1_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_USART1_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_USART1_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;
    GPIO_USART1_InitStructure.GPIO_Pin = USART1_RX_Pin | USART1_TX_Pin | USART1_CK_Pin;
    GPIO_Init(USART1_Port, &GPIO_USART1_InitStructure);

    // FIFO Init
    USART1_TXFIFO.itemSize = USART1_TXMSG_SIZE;
    USART1_TXFIFO.items = (void*) &USART1_TXFIFOData;
    USART1_TXFIFO.length = USART1_TXMSG_SIZE * USART1_TXFIFO_SIZE;
    FIFO_Init(&USART1_TXFIFO);

    // USART1 periph init
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

    USART1_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
    USART1_InitStructure.USART_BaudRate = USART1_BAUDRATE;
    USART1_InitStructure.USART_Parity = USART_Parity_No;
    USART1_InitStructure.USART_StopBits = USART_StopBits_1;
    USART1_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART1_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_Init(USART1, &USART1_InitStructure);

    USART1_ClockInitStructure.USART_CPHA = USART_CPHA_1Edge;
    USART1_ClockInitStructure.USART_CPOL = USART_CPOL_Low;
    USART1_ClockInitStructure.USART_Clock = USART_Clock_Enable;
    USART1_ClockInitStructure.USART_LastBit = USART_LastBit_Enable;
    USART_ClockInit(USART1, &USART1_ClockInitStructure);

    DMA1_Channel2_USART1TX_Init();
    DMA1_Channel2TCCallback = &USART1_DMATCCallback;
    USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
    USART_Cmd(USART1, ENABLE);
    USART1_TxStatus = USART1_TxStatus_Ready;
}

static void USART1_TransmitMsg(USART1_MsgTypeDef* msgPtr)
{
    uint8_t * from, *to, *end;

    if(USART1_TxStatus != USART1_TxStatus_Ready)
        return;

    // Copy data to DMA buffer
    from = (uint8_t*) msgPtr;
    to = USART1_TxData;
    end = to + USART1_TXMSG_SIZE;
    while(to < end)
        *to++ = *from++;

    // DMA init
    DMA_SetCurrDataCounter(DMA1_Channel2, USART1_TXMSG_SIZE);
    USART_ClearFlag(USART1, USART_FLAG_TC);
    DMA_ITConfig(DMA1_Channel2, DMA_IT_TC, ENABLE);

    // Activate DMA
    DMA_Cmd(DMA1_Channel2, ENABLE);
}

static void USART1_DMATCCallback(void)
{
    USART1_MsgTypeDef msg;

    if(USART1_TxStatus == USART1_TxStatus_Reset)
        return;

    CRITICALSECTION_Enter();
    {
        if(USART1_TxStatus == USART1_TxStatus_Sending)
            USART1_TxStatus = USART1_TxStatus_Ready;
        if(USART1_TXFIFO.cnt != 0)
        {
            FIFO_Pop(&USART1_TXFIFO, (void*) &msg);
            USART1_TransmitMsg(&msg);
        }
    }
    CRITICALSECTION_Exit();
}

ErrorStatus USART1_QueueMsg(USART1_MsgTypeDef* msgPtr)
{
    ErrorStatus ret = ERROR;
    CRITICALSECTION_Enter();
    {
        if(USART1_TxStatus == USART1_TxStatus_Sending)
        {
            ret = FIFO_Add(&USART1_TXFIFO, (void*) msgPtr);
        }
        else if(USART1_TxStatus == USART1_TxStatus_Ready)
        {
            USART1_TransmitMsg(msgPtr);
            ret = SUCCESS;
        }
    }
    CRITICALSECTION_Exit();
    return ret;
}

ErrorStatus USART1_SendData(uint8_t* data, uint8_t length)
{
    USART1_MsgTypeDef msg;
    uint16_t i;

    if(USART1_DATALENGTH < length)
        return ERROR;

    msg.Length = length;
    for(i = 0; i < length; i++)
        msg.Data[i] = data[i];

    /*/
     for(i = 0; i < length; i++)
     {
     while(USART_GetFlagStatus(USART1, USART_FLAG_TC) != SET)
     ;
     USART_SendData(USART1, data[i]);
     }
     return SUCCESS;
     //*/

    return USART1_QueueMsg(&msg);
}
