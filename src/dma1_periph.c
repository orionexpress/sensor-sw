/*
 * dma1_periph.c
 *
 *  Created on: 2015 nov. 7
 *      Author: palotasb
 */

#include "dma1_periph.h"

void (*DMA1_Channel1TCCallback)(void) = (void (*)(void))0;
void (*DMA1_Channel2TCCallback)(void) = (void (*)(void))0;
void (*DMA1_Channel3TCCallback)(void) = (void (*)(void))0;

void DMA1_Channel1_ADC1_Init(void)
{
    DMA_InitTypeDef DMA1_Channel1_InitStructure;

    DMA1_Channel1_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) & ADC1->DR;
    DMA1_Channel1_InitStructure.DMA_MemoryBaseAddr = (uint32_t) & ADC1_Data;
    DMA1_Channel1_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA1_Channel1_InitStructure.DMA_BufferSize = 8;
    DMA1_Channel1_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA1_Channel1_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA1_Channel1_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA1_Channel1_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
    DMA1_Channel1_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA1_Channel1_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA1_Channel1_InitStructure.DMA_M2M = DMA_M2M_Disable;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    // Ensure ADC DMA request is mapped to DMA channel 1
    SYSCFG_DMAChannelRemapConfig(SYSCFG_DMARemap_ADC1, DISABLE);
    DMA_Init(DMA1_Channel1, &DMA1_Channel1_InitStructure);
    DMA_Cmd(DMA1_Channel1, ENABLE);
    DMA_ITConfig(DMA1_Channel1, DMA_IT_TC, ENABLE);
}

void DMA1_Channel2_USART1TX_Init(void)
{
    DMA_InitTypeDef DMA1_Channel2_InitStructure;

    DMA1_Channel2_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) & USART1->TDR;
    DMA1_Channel2_InitStructure.DMA_MemoryBaseAddr = (uint32_t) & USART1_TxData;
    DMA1_Channel2_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA1_Channel2_InitStructure.DMA_BufferSize = USART1_TXMSG_SIZE;
    DMA1_Channel2_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA1_Channel2_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA1_Channel2_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA1_Channel2_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA1_Channel2_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA1_Channel2_InitStructure.DMA_Priority = DMA_Priority_Low;
    DMA1_Channel2_InitStructure.DMA_M2M = DMA_M2M_Disable;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    // Ensure ADC DMA request is mapped to DMA channel 1
    SYSCFG_DMAChannelRemapConfig(SYSCFG_DMARemap_USART1Tx, DISABLE);
    DMA_Init(DMA1_Channel2, &DMA1_Channel2_InitStructure);
    DMA_ITConfig(DMA1_Channel2, DMA_IT_TC, DISABLE);
    DMA_Cmd(DMA1_Channel2, DISABLE);
}

void DMA1_Channel3_SPI1TX_Init(void)
{
    DMA_InitTypeDef DMA1_Channel3_InitStructure;

    DMA1_Channel3_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) & SPI1->DR;
    DMA1_Channel3_InitStructure.DMA_MemoryBaseAddr = (uint32_t) & SPI1_TxDMABuff;
    DMA1_Channel3_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA1_Channel3_InitStructure.DMA_BufferSize = SPI1_FRAME_SIZE;
    DMA1_Channel3_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA1_Channel3_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA1_Channel3_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA1_Channel3_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA1_Channel3_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA1_Channel3_InitStructure.DMA_Priority = DMA_Priority_Low;
    DMA1_Channel3_InitStructure.DMA_M2M = DMA_M2M_Disable;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    // Ensure ADC DMA request is mapped to DMA channel 1
    DMA_Init(DMA1_Channel3, &DMA1_Channel3_InitStructure);
    DMA_ITConfig(DMA1_Channel3, DMA_IT_TC, DISABLE);
    DMA_Cmd(DMA1_Channel3, DISABLE);
}

void DMA1_Init(void)
{
    DMA1_Channel1_ADC1_Init();
    DMA1_Channel2_USART1TX_Init();
}
void DMA1_Channel2_TC(void)
{
    if(DMA1_Channel2TCCallback)
        DMA1_Channel2TCCallback();
}

void DMA1_Channel3_TC(void)
{
    if(DMA1_Channel3TCCallback)
        DMA1_Channel3TCCallback();
}
