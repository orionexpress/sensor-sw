/*
 * interrupts.c
 *
 *  Created on: 2015 nov. 7
 *      Author: palotasb
 */

#include "interrupts.h"

__attribute ((weak)) void IT_DefaultHandler(void);

__attribute ((weak, alias("IT_DefaultHandler"))) void SPI2_TXE(void);
__attribute ((weak, alias("IT_DefaultHandler"))) void SPI1_TXE(void);
__attribute ((weak, alias("IT_DefaultHandler"))) void SPI1_RXNE(void);
__attribute ((weak, alias("IT_DefaultHandler"))) void DMA1_Channel2_TC(void);
__attribute ((weak, alias("IT_DefaultHandler"))) void DMA1_Channel3_TC(void);
__attribute ((weak, alias("IT_DefaultHandler"))) void TIM14_UE(void);
__attribute ((weak, alias("IT_DefaultHandler"))) void TIM16_UE(void);

__attribute ((weak)) void IT_DefaultHandler(void)
{
}

void IT_Init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

    NVIC_InitStructure.NVIC_IRQChannel = SPI2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel2_3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPriority = 3;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = SPI1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPriority = 3;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = TIM16_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPriority = 2;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = TIM14_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPriority = 2;
    NVIC_Init(&NVIC_InitStructure);
}

void SPI1_IRQHandler(void)
{
    if(SPI_I2S_GetITStatus(SPI1, SPI_I2S_IT_TXE) == SET)
    {
        SPI1_TXE();
    }
    else if(SPI_I2S_GetITStatus(SPI1, SPI_I2S_IT_RXNE) == SET)
    {
        SPI1_RXNE();
    }
}

void SPI2_IRQHandler(void)
{
    if(SPI_I2S_GetITStatus(SPI2, SPI_I2S_IT_TXE) == SET)
    {
        SPI2_TXE();
    }
}

void DMA1_Channel2_3_IRQHandler(void)
{
    if(DMA_GetITStatus(DMA1_IT_TC2) == SET)
    {
        DMA_ClearITPendingBit (DMA1_IT_TC2);
        DMA1_Channel2_TC();
    }
    if(DMA_GetITStatus(DMA1_IT_TC3) == SET)
    {
        DMA_ClearITPendingBit (DMA1_IT_TC3);
        DMA1_Channel3_TC();
    }
}

void TIM14_IRQHandler(void)
{
    if(TIM_GetITStatus(TIM14, TIM_IT_Update) == SET)
    {
        TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
        TIM14_UE();
    }
}

void TIM16_IRQHandler(void)
{
    if(TIM_GetITStatus(TIM16, TIM_IT_Update) == SET)
    {
        TIM_ClearITPendingBit(TIM16, TIM_IT_Update);
        TIM16_UE();
    }
}
