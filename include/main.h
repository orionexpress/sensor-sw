/*
 * main.h
 *
 *  Created on: 2015 nov. 14
 *      Author: palotasb
 */

#ifndef MAIN_H_
#define MAIN_H_

#include "sensing.h"
#include "linetrack_modules.h"
#include "comgpio_periph.h"
#include "interrupts.h"
#include "debug.h"
#include "fifo.h"
#include "tim16_periph.h"

#endif /* MAIN_H_ */
