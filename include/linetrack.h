/*
 * linetrack.h
 *
 *  Created on: 2015 nov. 8
 *      Author: palotasb
 */

#ifndef LINETRACK_H_
#define LINETRACK_H_

#include "stm32f0xx.h"
#include "fixedpoint.h"
#include "fifo.h"
#include "criticalsection.h"
#include "debug.h"
#include "tim16_periph.h"
#include "util.h"

typedef enum
{
    LINETRACK_Reset = 0,
    LINETRACK_Calibrating,
    LINETRACK_PreCalculation,
    LINETRACK_Calculating,
    LINETRACK_CalculationReady
} LINETRACK_StatusTypeDef;

typedef struct
{
    uf24p8* TrackingPositions;
    uf24p8* FilteredTrackingPositions;
    uint32_t TrackingPositionCount;
    uint32_t TrackingPositionCapacity;

    uf24p8* SensorCalibrationLowValues;
    uf24p8* SensorCalibrationHighValues;

    /*FIFO* HistoricalTrackingPositions;
     uint32_t* HistoricalTrackingPositionData;*/

    int32_t TrackWidth;

    uf24p8* SensorData;
    uf24p8* SensorDataTmp;
    uint32_t SensorDataCount;
    uint8_t* SensorDataAboveDetectionThreshold;

    uf24p8 DetectionThreshold;

    FunctionalState UseSensorCalibration;
    FunctionalState PreDetectionFIRFilter;
    FunctionalState PreDetectionMedianFilter;
    FunctionalState FIRFilter;

    /*uint32_t* FIRFilterElements;
     uint32_t FIRFilterLength;*/

    LINETRACK_StatusTypeDef Status;
} LINETRACK_ModuleTypeDef;

void LINETRACK_Init(LINETRACK_ModuleTypeDef * module);
ErrorStatus LINETRACK_Calibrate(LINETRACK_ModuleTypeDef * module);
ErrorStatus LINETRACK_Calculate(LINETRACK_ModuleTypeDef * module);

#endif /* LINETRACK_H_ */
