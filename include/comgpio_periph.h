/*
 * comgpio_periph.h
 *
 *  Created on: 2015 nov. 14
 *      Author: palotasb
 */

#ifndef COMGPIO_PERIPH_H_
#define COMGPIO_PERIPH_H_

#include "stm32f0xx.h"
#include "debug.h"

#define COM_GPIO_Port   GPIOB
#define COM_GPIO_Pin    GPIO_Pin_9
#define COM_GPIO_RCC_BusPeriphClockCmd RCC_AHBPeriphClockCmd
#define COM_GPIO_RCC_BusPeriphPortENR RCC_AHBENR_GPIOBEN

void COM_GPIO_OutputInit(void);
void COM_GPIO_InputInit(void);
void COM_GPIO_On(void);
void COM_GPIO_Off(void);
uint8_t COM_GPIO_GetValue(void);

#endif /* COMGPIO_PERIPH_H_ */
