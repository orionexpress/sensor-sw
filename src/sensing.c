/*
 * sensing.c
 *
 *  Created on: 2015 nov. 8
 *      Author: palotasb
 */

#include "sensing.h"

#define SENSING_SUBPHASE_Mask   0x07
#define SENSING_SUBPHASE_Pos    3
#define SENSING_SUBPHASE_Count  (1 << SENSING_SUBPHASE_Pos)

SENSING_StateTypeDef SENSING_State;

// The maximum number of phases to output. 0xFFFFFFFF for infinity.
static uint32_t SENSING_CounterMax = 1;

// The readout sequence
const uint8_t SENSING_MeasurementSequence[8] =
{ 0, 5, 2, 7, 4, 1, 6, 3 };

// For 5-bit shifting and readout sequence.
const uint8_t SENSING_ShiftOutPhase[8] =
{ 0x01, 0x00, 0x04, 0x00, 0x10, 0x02, 0x00, 0x08 };

// The last phase associated with the shifting pattern above
const uint32_t SENSING_ShiftOutPreload[2] =
{ 0x08080808, 0x08080808 };

FunctionalState SENSING_SwitchOffWhileShifting = ENABLE;
FunctionalState SENSING_AutoSwitchOffAtEndOfConversion = ENABLE;
FunctionalState SENSING_AutoEnableAfterPreload = ENABLE;
uint32_t* SENSING_SensorData1 = (uint32_t*) 0;
uint32_t* SENSING_SensorData2 = (uint32_t*) 0;
uint32_t SENSING_MeasurementCounter = RESET;

static void SENSING_Shift(uint32_t phase);
static void SENSING_PhaseComplete(uint32_t phase);
static void SENSING_WaitForLevel(void);
static void SENSING_WaitForConversion(void);

void SENSING_Init(void)
{
    SENSING_State = SENSING_State_Reset;
    SENSING_MeasurementCounter = RESET;

    SPI2_Init();
    ADC1_Init();
}

ErrorStatus SENSING_FullMeasurement(void)
{
    uint32_t meas, phase;

    if(SENSING_State != SENSING_State_Reset)
        return ERROR;
    SENSING_MeasurementCounter = RESET;

    for(meas = 0; (meas < SENSING_CounterMax) || (SENSING_CounterMax == UINT32_MAX); meas++)
    {
        SPI2_SendSensPreloadData(SENSING_ShiftOutPreload[0], SENSING_ShiftOutPreload[1]);

        for(phase = 0; phase < SENSING_SUBPHASE_Count; phase++)
        {
            SENSING_Shift(phase);
            SPI2_SensOutputCmd (ENABLE);
            SENSING_WaitForLevel();
            ADC1_StartConversion();
            SENSING_WaitForConversion();
            SPI2_SensOutputCmd (DISABLE);
            SENSING_PhaseComplete(phase);
        }

        SENSING_MeasurementCounter++;
    }

    return SUCCESS;
}

void SENSING_MaxRepeatCmd(uint32_t maxCount)
{
    if(maxCount & 0xE0000000UL)
        SENSING_CounterMax = UINT32_MAX;
    else
        SENSING_CounterMax = maxCount;
}

static void SENSING_Shift(uint32_t phase)
{
    if(SENSING_SwitchOffWhileShifting)
        SPI2_SensOutputCmd (DISABLE);

    SPI2_ShiftSensData(SENSING_ShiftOutPhase[phase]);
}

static void SENSING_WaitForLevel(void)
{
    uint32_t start = TIM14_GetCnt();
    while(TIM14_GetCnt() - start < SENSING_LevelingTime)
        ;
}

static void SENSING_WaitForConversion(void)
{
    while(DMA_GetFlagStatus(DMA1_FLAG_TC1) == RESET)
        ;
}

static void SENSING_PhaseComplete(uint32_t phase)
{
    uint32_t i;

    // Clear AD-copying DMA channel's Transfer Complete flag
    DMA_ClearFlag (DMA1_FLAG_TC1);

    // Unshuffle out of order measurements
    for(i = 0; i < 4; i++)
    {
        if(SENSING_SensorData1)
            SENSING_SensorData1[8 * i + SENSING_MeasurementSequence[phase]] = ADC1_Data[0 + i];
    }
    for(i = 0; i < 4; i++)
    {
        if(SENSING_SensorData2)
            SENSING_SensorData2[8 * i + SENSING_MeasurementSequence[phase]] = ADC1_Data[4 + i];
    }
}

