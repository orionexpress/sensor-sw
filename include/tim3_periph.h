/*
 * tim3_periph.h
 *
 *  Created on: 2015 nov. 16
 *      Author: palotasb
 */

#ifndef TIM3_PERIPH_H_
#define TIM3_PERIPH_H_

#include "stm32f0xx.h"

void TIM3_Init(void);
void TIM3_SetOutput(uint8_t);

#endif /* TIM3_PERIPH_H_ */
