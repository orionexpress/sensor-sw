/*
 * spi2_periph.h
 *
 *  Created on: 2015 nov. 6
 *      Author: palotasb
 */

#ifndef SPI2_PERIPH_H_
#define SPI2_PERIPH_H_

#include "stm32f0xx.h"
#include "criticalsection.h"
#include "fifo.h"
#include "debug.h"

#define SPI2_COMMON_Port        GPIOB
#define SPI2_LED_OE_Port        GPIOB
#define SPI2_LED_OE_Pin         GPIO_Pin_1
#define SPI2_LED_OE_PinSource   GPIO_PinSource1
#define SPI2_LED_LE_Port        GPIOB
#define SPI2_LED_LE_Pin         GPIO_Pin_10
#define SPI2_LED_LE_PinSource   GPIO_PinSource10
#define SPI2_SENS_OE_Port       GPIOB
#define SPI2_SENS_OE_Pin        GPIO_Pin_11
#define SPI2_SENS_OE_PinSource  GPIO_PinSource11
#define SPI2_SENS_LE_Port       GPIOB
#define SPI2_SENS_LE_Pin        GPIO_Pin_12
#define SPI2_SENS_LE_PinSource  GPIO_PinSource12
#define SPI2_SCK_Port           GPIOB
#define SPI2_SCK_Pin            GPIO_Pin_13
#define SPI2_SCK_PinSource      GPIO_PinSource13
#define SPI2_MISO_Port          GPIOB
#define SPI2_MISO_Pin           GPIO_Pin_14
#define SPI2_MISO_PinSource     GPIO_PinSource14
#define SPI2_MOSI_Port          GPIOB
#define SPI2_MOSI_Pin           GPIO_Pin_15
#define SPI2_MOSI_PinSource     GPIO_PinSource15

#define SPI2_SHIFT_BITS         5
#define SPI2_SHIFT_MASK         0x1F

#define SPI2_LATCH_TO           50

typedef enum
{
    SPI2_State_Reset = 0,
    SPI2_State_Ready,
    SPI2_State_Waiting
} SPI2_StateTypeDef;

extern void (*SPI2_TransmitEndedCallback)(void);

void SPI2_Init(void);
void SPI2_SendSensPreloadData(uint32_t line1, uint32_t line2);
void SPI2_SendDebugLedData(uint32_t line1);
void SPI2_ShiftSensData(uint8_t data);
void SPI2_SensOutputCmd(FunctionalState);
void SPI2_LEDOutputCmd(FunctionalState);

static inline void SPI2_WaitWhileBusy(void)
{
    while(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET)
        ;
}

#endif /* SPI2_PERIPH_H_ */
