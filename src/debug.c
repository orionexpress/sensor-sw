/*
 * debug.c
 *
 *  Created on: 2015 nov. 16
 *      Author: palotasb
 */

#include "debug.h"

//#define DEBUG_NO_ITM

uint32_t DEBUG_Leds = 0UL;
static uint8_t hex_array[16] =
{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

void DEBUG_SetLeds(uint32_t leds)
{
    DEBUG_Leds |= leds;
    DEBUG_Update();
}

void DEBUG_ResetLeds(uint32_t leds)
{
    DEBUG_Leds &= ~leds;
    DEBUG_Update();
}

void DEBUG_ToggleLeds(uint32_t leds)
{
    DEBUG_Leds ^= leds;
    DEBUG_Update();
}

void DEBUG_Update(void)
{
    SPI2_SendDebugLedData(DEBUG_Leds);
}

void DEBUG_Init(void)
{
    SPI2_Init();
    TIM3_Init();
    DEBUG_SetLight(127);
}

void DEBUG_SetLight(uint8_t light)
{
    TIM3_SetOutput(light);
}

void DEBUG_Write(char* str)
{
#if !defined(DEBUG_NO_ITM)
    uint32_t i = 0;
    while(str[i])
    {
        if((i % 8) == 7)
            trace_write(str + i - 7, 8);
        i++;
    }
    trace_write(str + (i & 0xFFFFFFF8), (i & 0x7));
#endif
}

void DEBUG_WriteU8(uint8_t value)
{
#if !defined(DEBUG_NO_ITM)
    uint8_t c[2];

    c[0] = hex_array[(value & 0xF0) >> 4];
    c[1] = hex_array[(value & 0x0F)];

    trace_write(&c, 2);
#endif
}

void DEBUG_WriteU8D(uint8_t value)
{
#if !defined(DEBUG_NO_ITM)
    uint8_t c[3];

    c[2] = hex_array[value % 10];
    if(value / 10)
        c[1] = hex_array[(value / 10) % 10];
    else
        c[1] = ' ';
    if(value / 100)
        c[0] = hex_array[(value / 100) % 10];
    else
        c[0] = ' ';

    trace_write(c, 3);
#endif
}

void DEBUG_WriteU16D(uint16_t value)
{
#if !defined(DEBUG_NO_ITM)
    uint8_t c[5];

    c[4] = hex_array[value % 10];

    if(value / 10)
        c[3] = hex_array[(value / 10) % 10];
    else
        c[3] = ' ';

    if(value / 100)
        c[2] = hex_array[(value / 100) % 10];
    else
        c[2] = ' ';

    if(value / 1000)
        c[1] = hex_array[(value / 1000) % 10];
    else
        c[1] = ' ';

    if(value / 10000)
        c[0] = hex_array[(value / 10000) % 10];
    else
        c[0] = ' ';

    trace_write(c, 5);
#endif
}

void DEBUG_WriteU16(uint16_t value)
{
    DEBUG_WriteU8((value & 0xFF00) >> 8);
    DEBUG_WriteU8((value & 0x00FF) >> 0);
}

void DEBUG_WriteU32(uint32_t value)
{
    DEBUG_WriteU8((value & 0xFF000000) >> 24);
    DEBUG_WriteU8((value & 0x00FF0000) >> 16);
    DEBUG_WriteU8((value & 0x0000FF00) >> 8);
    DEBUG_WriteU8((value & 0x000000FF) >> 0);
}
