/*
 * _hw_init.c
 *
 *  Created on: 2015 dec. 3
 *      Author: palotasb
 */

#include "stm32f0xx.h"

void __initialize_hardware_early(void);

void __initialize_hardware_early(void)
{
    /* Set HSION bit */
    RCC->CR |= (uint32_t) 0x00000001;

#if defined (STM32F031) || defined (STM32F072) || defined (STM32F042)
    /* Reset SW[1:0], HPRE[3:0], PPRE[2:0], ADCPRE and MCOSEL[2:0] bits */
    RCC->CFGR &= (uint32_t)0xF8FFB80C;
#else
    /* Reset SW[1:0], HPRE[3:0], PPRE[2:0], ADCPRE, MCOSEL[2:0], MCOPRE[2:0] and PLLNODIV bits */
    RCC->CFGR &= (uint32_t) 0x08FFB80C;
#endif /* STM32F031*/

    /* Reset HSEON, CSSON and PLLON bits */
    RCC->CR &= (uint32_t) 0xFEF6FFFF;

    /* Reset HSEBYP bit */
    RCC->CR &= (uint32_t) 0xFFFBFFFF;

    /* Reset PLLSRC, PLLXTPRE and PLLMUL[3:0] bits */
    RCC->CFGR &= (uint32_t) 0xFFC0FFFF;

    /* Reset PREDIV1[3:0] bits */
    RCC->CFGR2 &= (uint32_t) 0xFFFFFFF0;

    /* Reset USARTSW[1:0], I2CSW, CECSW and ADCSW bits */
    RCC->CFGR3 &= (uint32_t) 0xFFFFFEAC;

    /* Reset HSI14 bit */
    RCC->CR2 &= (uint32_t) 0xFFFFFFFE;

    /* Disable all interrupts */
    RCC->CIR = 0x00000000;

    /* ------- HSI PLL config -------- */

    RCC->CR &= 0xFEFFFFFFUL;  // PLLON = 0
    while(RCC->CR & 0x02000000UL)
        // wait for PLLRDY = 0
        ;

    RCC->CFGR |= 0x00280000UL;  // PLLMUL = 1010 (PLL x12)

    RCC->CR |= 0x01000000UL;  // PLLON = 1
    while(!(RCC->CR & 0x02000000UL))
        // Wait for PLLRDY = 1
        ;

    RCC->CFGR |= 0x00000002UL;  // SW[1:0] = 10 // Select PLL as system clock source

    while((RCC->CFGR & 0x0000000CUL) != 0x00000008UL)
        // Wait for SWS[1:0] = 10 // PLL selected as system clock source
        ;

}
