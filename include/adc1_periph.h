/*
 * adc1_periph.h
 *
 *  Created on: 2015 nov. 7
 *      Author: palotasb
 */

#ifndef ADC1_PERIPH_H_
#define ADC1_PERIPH_H_

#include "stm32f0xx.h"
#include "debug.h"

extern uint32_t ADC1_Data[8];

void ADC1_Init(void);
void ADC1_StartConversion(void);

#endif /* ADC1_PERIPH_H_ */
