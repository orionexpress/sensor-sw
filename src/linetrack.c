/*
 * linetrack.c
 *
 *  Created on: 2015 nov. 8
 *      Author: palotasb
 */

#include "linetrack.h"

#define DET_THRES_ADD   (10000)
#define DET_THRES_MUL   (0x0140)

static void LINETRACK_PreDetectionMedianFilter(LINETRACK_ModuleTypeDef * module);
static void LINETRACK_PreDetectionFIRFilter(LINETRACK_ModuleTypeDef * module);
static void LINETRACK_LineDetection(LINETRACK_ModuleTypeDef * module);
static void LINETRACK_CoarseLineTracking(LINETRACK_ModuleTypeDef * module);
static void LINETRACK_FineLineTracking(LINETRACK_ModuleTypeDef * module);
static void LINETRACK_Filter(LINETRACK_ModuleTypeDef * module);
static void LINETRACK_CalibrateSensorValues(LINETRACK_ModuleTypeDef * module);
static uint32_t LINETRACK_GroundMedian(LINETRACK_ModuleTypeDef * module);

/* Calibration:
 * y = measured value
 * x = real value
 * a = measured lowest value (white) = 0<<8 ideally
 * b = measured highest value (black) = 255<<8 ideally
 * linear transform:
 * y = (b - a) / 255 * x + a = x ideally
 * x = 255 * (y - a) / (b - a) = y ideally
 */

void LINETRACK_Init(LINETRACK_ModuleTypeDef * module)
{
    uint32_t i;
    for(i = 0; i < module->SensorDataCount; i++)
        module->SensorDataTmp[i] = 0;
    module->Status = LINETRACK_Reset;
}

ErrorStatus LINETRACK_Calculate(LINETRACK_ModuleTypeDef * module)
{
    if(module->Status != LINETRACK_PreCalculation && module->Status != LINETRACK_CalculationReady)
        return ERROR;

    module->Status = LINETRACK_PreCalculation;

    if(module->UseSensorCalibration == ENABLE)
        LINETRACK_CalibrateSensorValues(module);

    // module->SensorDataTmp[] now contains the median-filtered version of module->SensorData[]
    if(module->PreDetectionMedianFilter)
        LINETRACK_PreDetectionMedianFilter(module);

    // module->SensorDataTmp[] now contains the FIR-filtered version of module->SensorData[]
    if(module->PreDetectionMedianFilter)
        LINETRACK_PreDetectionFIRFilter(module);

    // module->SensorDataAboveDetectionThreshold[] is now for each index above/below detection threshold
    LINETRACK_LineDetection(module);

    // module->TrackingPositions[] now contains the indices of the sensors which are above detection threshold
    LINETRACK_CoarseLineTracking(module);

    // module->TrackingPositions[] now contains the 24.8bit fixedpoint
    // "sub-sensor indices" where the tracked lines are
    LINETRACK_FineLineTracking(module);

    if(module->FIRFilter == ENABLE)
        LINETRACK_Filter(module);

    module->Status = LINETRACK_CalculationReady;
    return SUCCESS;
}

static void LINETRACK_CalibrateSensorValues(LINETRACK_ModuleTypeDef * module)
{
    uint32_t i;
    for(i = 0; i < module->SensorDataCount; i++)
    {
        if(module->SensorData[i] <= module->SensorCalibrationLowValues[i])
            module->SensorData[i] = 0;
        else if(module->SensorCalibrationHighValues[i] <= module->SensorData[i])
            module->SensorData[i] = 0xFF00;
        else
            module->SensorData[i] = ((255 * (module->SensorData[i] - module->SensorCalibrationLowValues[i])) << 8)
                    / ((module->SensorCalibrationHighValues[i] - module->SensorCalibrationLowValues[i]));
        // See Calibration comment at top of file.
        // uf24.8 = ((uf24.8 - uf24.8) << 8) / (uf24.8 - uf24.8) = uf32.16 / uf24.8
    }
}

ErrorStatus LINETRACK_Calibrate(LINETRACK_ModuleTypeDef * module)
{
    if(module->UseSensorCalibration == ENABLE)
    {
        LINETRACK_CalibrateSensorValues(module);
    }

    module->DetectionThreshold = ((LINETRACK_GroundMedian(module) * DET_THRES_MUL) >> 8) + DET_THRES_ADD;

    module->Status = LINETRACK_PreCalculation;
    return SUCCESS;
}

static uint32_t LINETRACK_GroundMedian(LINETRACK_ModuleTypeDef * module)
{
    uint32_t i, median;

    for(i = 0; i < module->SensorDataCount; i++)
    {
        module->SensorDataTmp[i] = module->SensorData[i];
    }

    quick_sort_u32(module->SensorDataTmp, module->SensorDataCount);

    median = module->SensorDataTmp[module->SensorDataCount / 2];

    return median;
}

static void LINETRACK_PreDetectionMedianFilter(LINETRACK_ModuleTypeDef * module)
{
    uint32_t sensorIndex, minValue, maxValue;
    uint32_t j;
    int32_t minIndex, maxIndex, medIndex;

    module->SensorDataTmp[0] = module->SensorData[0];
    module->SensorDataTmp[module->SensorDataCount - 1] = module->SensorData[module->SensorDataCount - 1];

    for(sensorIndex = 1; sensorIndex < module->SensorDataCount - 1; sensorIndex++)
    {
        minValue = module->SensorData[sensorIndex - 1];
        maxValue = module->SensorData[sensorIndex - 1];
        minIndex = -1;
        maxIndex = -1;
        for(j = 0; j <= 1; j++)
        {
            // 3-wide median filter algorithm:
            // maxIndex is the index of the first smallest item in the window
            // minIndex is the index of the last largest item in the window
            // medIndex is the index of the median element
            // maxIndex + medIndex + minIndex = constant 0, imed can be calculated
            if(module->SensorData[sensorIndex + j] < minValue)
            {
                minValue = module->SensorData[sensorIndex + j];
                minIndex = j;
            }
            else if(maxValue <= module->SensorData[sensorIndex + j])
            {
                maxValue = module->SensorData[sensorIndex + j];
                maxIndex = j;
            }
        }
        medIndex = 0 - minIndex - maxIndex;
        module->SensorDataTmp[sensorIndex] = module->SensorData[sensorIndex + medIndex];
    }
}

static void LINETRACK_LineDetection(LINETRACK_ModuleTypeDef * module)
{
    uint32_t i;
    uint32_t * DataPtr;

    if(module->PreDetectionMedianFilter == ENABLE)
    {
        DataPtr = module->SensorDataTmp;
    }
    else
    {
        DataPtr = module->SensorData;
    }

    for(i = 0; i < module->SensorDataCount; i++)
    {
        if(module->DetectionThreshold <= DataPtr[i])
        {
            module->SensorDataAboveDetectionThreshold[i] = 1;
        }
        else
        {
            module->SensorDataAboveDetectionThreshold[i] = 0;
        }
    }
}

static void LINETRACK_CoarseLineTracking(LINETRACK_ModuleTypeDef * module)
{
    uint32_t sensorIndex, maxVal, maxIndex;

    module->TrackingPositionCount = 0;

    // Loop thru sensor data
    // Add maximum indices to TrackingPositions
    for(sensorIndex = 0; sensorIndex < module->SensorDataCount; sensorIndex++)
    {
        if(module->SensorDataAboveDetectionThreshold[sensorIndex])
        {
            maxVal = module->SensorData[sensorIndex];
            maxIndex = sensorIndex;
            do
            {
                if(maxVal < module->SensorData[sensorIndex])
                {
                    maxVal = module->SensorData[sensorIndex];
                    maxIndex = sensorIndex;
                }
            } while(module->SensorDataAboveDetectionThreshold[sensorIndex++]);

            sensorIndex--;  // Thr for(;;) will increase this.

            module->TrackingPositions[module->TrackingPositionCount++] = maxIndex;
            if(module->TrackingPositionCount == module->TrackingPositionCapacity)
                break;
        }
    }
}

static void LINETRACK_FineLineTracking(LINETRACK_ModuleTypeDef * module)
{
    uint32_t tpIndex, tpCenterIndex;
    int32_t tpWeightIndex, tpWeightIndexSat;
    uf24p8 tpWeight, tpValue, tpResult, tpWeightSum;

    // Getting a finer approximation of the line position based on the averages of several sensors

    // Loop thru coarse tracking positions (based on a simple threshold detection)
    // Each tracking position contains the sensor where the maximal value was measured
    for(tpIndex = 0; tpIndex < module->TrackingPositionCount; tpIndex++)
    {
        tpCenterIndex = module->TrackingPositions[tpIndex];
        module->TrackingPositions[tpIndex] = 0;
        tpWeightSum = 0;
        tpValue = 0;    // 24+8 bit fixedpoint "index" of tracking position

        // TODO: itt amúgy az egyszerű súlyozott átlagnál jobbat kellene majd mondani
        // mert az nem a burkológörbe maximumhelyét adja pontosan

        // Loop thru the neighbouring sensor values to do the weight-based averageing
        for(tpWeightIndex = -(module->TrackWidth / 2); tpWeightIndex <= module->TrackWidth / 2; tpWeightIndex++)
        {
            tpWeightIndexSat = tpCenterIndex + tpWeightIndex;
            if(tpWeightIndexSat < 0)
                tpWeightIndexSat = 0;
            else if(module->SensorDataCount - 1 < tpWeightIndexSat)
                tpWeightIndexSat = module->SensorDataCount - 1;
            // tpWeightIndexSat: saturated index for the sensor data

            // uf24.8 += uf32.0 * uf24.8
            tpWeight = module->SensorData[tpWeightIndexSat];
            tpValue += tpWeightIndexSat * tpWeight;
            // uf24.8
            tpWeightSum += tpWeight;
        }

        // uf24.8 = (uf24.8 << 8) / uf24.8 = uf24.16 / uf24.8
        tpResult = (tpValue << 8) / tpWeightSum;
        module->TrackingPositions[tpIndex] = tpResult;
    }
}

static void LINETRACK_PreDetectionFIRFilter(LINETRACK_ModuleTypeDef * module)
{

}

static void LINETRACK_Filter(LINETRACK_ModuleTypeDef * module)
{

}
