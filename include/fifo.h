/*
 * fifo.h
 *
 *  Created on: 2015 nov. 6
 *      Author: palotasb
 */

#ifndef FIFO_H_
#define FIFO_H_

#include "criticalsection.h"
#include "stm32f0xx.h"
#include "util.h"

typedef struct
{
    unsigned int length;
    unsigned int itemSize;
    unsigned int in;
    unsigned int out;
    unsigned int cnt;
    void * items;
} FIFO;

void FIFO_Init(FIFO* fifo);
ErrorStatus FIFO_Add(FIFO* fifo, void* item);
ErrorStatus FIFO_Pop(FIFO* fifo, void* item);
ErrorStatus FIFO_Poll(FIFO* fifo, void* item);
ErrorStatus FIFO_PollBack(FIFO* fifo, unsigned int back, void** item);

#endif /* FIFO_H_ */
