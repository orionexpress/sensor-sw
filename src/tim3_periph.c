/*
 * tim3_periph.c
 *
 *  Created on: 2015 nov. 16
 *      Author: palotasb
 */

#include "tim3_periph.h"

void TIM3_Init(void)
{
    TIM_TimeBaseInitTypeDef TIM3_InitStructure;
    TIM_OCInitTypeDef TIM3_OCInitStructure;
    GPIO_InitTypeDef GPIOPB1_InitStructure;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

    TIM3_InitStructure.TIM_Prescaler = 0;  // 1 MHz tick
    TIM3_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM3_InitStructure.TIM_ClockDivision = TIM_CKD_DIV4;
    TIM3_InitStructure.TIM_RepetitionCounter = 0;
    TIM3_InitStructure.TIM_Period = 255;

    TIM_TimeBaseInit(TIM3, &TIM3_InitStructure);

    TIM_OCStructInit(&TIM3_OCInitStructure);

    TIM3_OCInitStructure.TIM_Pulse = 255;
    TIM3_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM3_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM3_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

    TIM_OC4Init(TIM3, &TIM3_OCInitStructure);
    TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable);

    GPIO_StructInit(&GPIOPB1_InitStructure);
    GPIOPB1_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIOPB1_InitStructure.GPIO_Pin = GPIO_Pin_1;

    GPIO_PinAFConfig(GPIOB, GPIO_PinSource1, GPIO_AF_1);

    GPIO_Init(GPIOB, &GPIOPB1_InitStructure);
    TIM_Cmd(TIM3, ENABLE);
}

void TIM3_SetOutput(uint8_t output)
{
    TIM3->CCR4 = output;
}

