/*
 * fifo.c
 *
 *  Created on: 2015 nov. 6
 *      Author: palotasb
 */

#include "fifo.h"

void* FIFO_GetItemByIndex(FIFO* fifo, uint32_t index);

void * FIFO_GetItemByIndex(FIFO* fifo, uint32_t index)
{
    return (void *) ((unsigned char *) (fifo->items) + index * fifo->itemSize);
}

void FIFO_Init(FIFO* fifo)
{
    fifo->in = 0;
    fifo->out = 0;
    fifo->cnt = 0;
}

ErrorStatus FIFO_Add(FIFO* fifo, void* item)
{
    if(fifo->length == fifo->cnt)
        return ERROR;

    CRITICALSECTION_Enter();

    CopyData(item, FIFO_GetItemByIndex(fifo, fifo->in), fifo->itemSize);
    fifo->in++;
    if(fifo->in == fifo->length)
        fifo->in = 0;
    fifo->cnt++;

    CRITICALSECTION_Exit();

    return SUCCESS;
}

ErrorStatus FIFO_Pop(FIFO* fifo, void* item)
{
    if(!fifo->cnt)
        return ERROR;

    CRITICALSECTION_Enter();

    CopyData(FIFO_GetItemByIndex(fifo, fifo->out), item, fifo->itemSize);
    fifo->out++;
    if(fifo->out == fifo->length)
        fifo->out = 0;
    fifo->cnt--;

    CRITICALSECTION_Exit();

    return SUCCESS;
}

ErrorStatus FIFO_Poll(FIFO* fifo, void* item)
{
    if(!fifo->cnt)
        return ERROR;

    CRITICALSECTION_Enter();

    CopyData(FIFO_GetItemByIndex(fifo, fifo->out), item, fifo->itemSize);

    CRITICALSECTION_Exit();

    return SUCCESS;
}

ErrorStatus FIFO_PollBack(FIFO* fifo, unsigned int back, void** item)
{
    unsigned int index;

    if(fifo->cnt <= back)
        return ERROR;

    if(fifo->out < back)
    {
        index = fifo->length - fifo->out + back;
    }
    else
    {
        index = fifo->out - back;
    }

    *item = FIFO_GetItemByIndex(fifo, index);
    return SUCCESS;
}
