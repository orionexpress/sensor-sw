/*
 * tim16_periph.c
 *
 *  Created on: 2015 dec. 2
 *      Author: palotasb
 */

#define TIM16_TIMERCOUNT     8

#include "tim16_periph.h"

volatile uint32_t TIM16_Timers[TIM16_TIMERCOUNT];
uint32_t mshw = 0;

void TIM16_Init(void)
{
    TIM_TimeBaseInitTypeDef TIM16_InitStructure;

    TIM_DeInit (TIM16);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM16, ENABLE);

    TIM16_InitStructure.TIM_Prescaler = 47;  // 1 MHz tick
    TIM16_InitStructure.TIM_CounterMode = TIM_CounterMode_Down;
    TIM16_InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM16_InitStructure.TIM_RepetitionCounter = 0;
    TIM16_InitStructure.TIM_Period = 1000;  // 1 kHz period

    TIM_TimeBaseInit(TIM16, &TIM16_InitStructure);

    TIM_ITConfig(TIM16, TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM16, ENABLE);
}

uint32_t TIM16_GetCnt()
{
    uint32_t res;
    res = (mshw << 16) | (TIM16->CNT & 0xFFFFUL);
    return res;
}

void TIM16_Wait(uint32_t id, uint32_t time)
{
    if(TIM16_TIMERCOUNT <= id)
        return;

    TIM16_Timers[id] = time;

    while(TIM16_Timers[id])
        ;
}

void TIM16_SetTimer(uint32_t id, uint32_t time)
{
    if(TIM16_TIMERCOUNT <= id)
        return;

    TIM16_Timers[id] = time;
}

int32_t TIM16_Timeout(uint32_t id)
{
    if(TIM16_TIMERCOUNT <= id)
        return -1;
    if(TIM16_Timers[id] == 0)
        return 1;
    return 0;
}

void TIM16_UE(void)
{
    uint32_t i;

    mshw++;
    mshw &= 0xFFFFUL;

    for(i = 0; i < TIM16_TIMERCOUNT; i++)
    {
        if(TIM16_Timers[i])
            TIM16_Timers[i]--;
    }
}
