/*
 * tim16_periph.h
 *
 *  Created on: 2015 dec. 2
 *      Author: palotasb
 */

#ifndef TIM16_PERIPH_H_
#define TIM16_PERIPH_H_

#include "stm32f0xx.h"

void TIM16_Init(void);
uint32_t TIM16_GetCnt(void);
void TIM16_SetTimer(uint32_t id, uint32_t time);
int32_t TIM16_Timeout(uint32_t id);
void TIM16_Wait(uint32_t id, uint32_t time);
void TIM16_UE(void);

#endif /* TIM6_PERIPH_H_ */
