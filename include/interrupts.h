/*
 * interrupts.h
 *
 *  Created on: 2015 nov. 7
 *      Author: palotasb
 */

#ifndef INTERRUPTS_H_
#define INTERRUPTS_H_

#include "stm32f0xx.h"

void IT_Init(void);

void SPI2_IRQHandler(void);
void SPI1_IRQHandler(void);
void DMA1_Channel1_IRQHandler(void);
void DMA1_Channel2_3_IRQHandler(void);
void TIM14_IRQHandler(void);
void TIM15_IRQHandler(void);
void TIM16_IRQHandler(void);

#endif /* INTERRUPTS_H_ */
