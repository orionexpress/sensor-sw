/*
 * serialmessaging.c
 *
 *  Created on: 2015 dec. 8
 *      Author: palotasb
 */

#include "serialmessaging.h"

#define SM_NUM  16

uint8_t* SM_BufferPointers[SM_NUM] =
{ 0 };
void (*SM_FunctionPointers[SM_NUM])(uint8_t, uint8_t, uint8_t*) =
{   0};

SM_StateTypeDef SM_State;
static uint8_t msgType;
static int32_t msgCounter;
static uint8_t msgLength;

void SM_Init(void)
{
    uint32_t i;
    for(i = 0; i < SM_NUM; i++)
    {
        SM_BufferPointers[i] = (uint8_t *) 0;
        SM_FunctionPointers[i] = ((void (*)(uint8_t, uint8_t, uint8_t*)) 0);
    }

    SPI1_RXNECallback = &SM_Receive;

    SPI1_Init();
    SM_State = SM_State_Ready;
}

void SM_Receive(uint8_t data)
{
    if(SM_State == SM_State_Ready)
    {
        msgType = data;
        msgCounter = -1;
        if(SM_FunctionPointers[msgType])
            SM_State = SM_State_Receiving;
    }
    else if(SM_State == SM_State_Receiving)
    {
        if(msgCounter == -1)
        {
            msgLength = data;
            msgCounter = 0;
        }
        if(msgCounter == msgLength)
        {
            if(SM_FunctionPointers[msgType])
                SM_FunctionPointers[msgType](msgType, msgLength, SM_BufferPointers[msgType]);
            SM_State = SM_State_Ready;
        }
        else
        {
            SM_BufferPointers[msgType][msgCounter++] = data;
        }
    }
}

void SM_Register(uint8_t type, uint8_t* buffer, void (*fn)(uint8_t, uint8_t, uint8_t*))
{
    if(type < SM_NUM)
    {
        SM_BufferPointers[type] = buffer;
        SM_FunctionPointers[type] = fn;
    }
}

void SM_Unregister(uint8_t type)
{
    if(type < SM_NUM)
    {
        SM_BufferPointers[type] = (uint8_t *) 0;
        SM_FunctionPointers[type] = ((void (*)(uint8_t, uint8_t, uint8_t*)) 0);
    }
}

void SM_Send(uint8_t type, uint8_t length, uint8_t* data)
{
    SPI1_Frame frame;
    uint32_t i;
    frame.type = type;
    frame.length = length;
    for(i = 0; i < SPI1_DATA_SIZE; i++)
    {
        frame.data[i] = data[i];
    }
    SPI1_SendData(&frame);
}
