/*
 * tim14_periph.c
 *
 *  Created on: 2015 dec. 12
 *      Author: palotasb
 */

#include "tim14_periph.h"

uint32_t TIM14_HighHalfWord;

void TIM14_Init(void)
{
    TIM_TimeBaseInitTypeDef TIM14_InitStructure;

    TIM_DeInit (TIM14);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);

    TIM14_InitStructure.TIM_Prescaler = 47;  // 1 MHz tick
    TIM14_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM14_InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM14_InitStructure.TIM_RepetitionCounter = 0;
    TIM14_InitStructure.TIM_Period = 0xFFFF;  // Full period

    TIM_TimeBaseInit(TIM14, &TIM14_InitStructure);

    TIM_ITConfig(TIM14, TIM_IT_Update, ENABLE);
    TIM_Cmd(TIM14, ENABLE);
}

void TIM14_UE(void)
{
    CRITICALSECTION_FastEnter();
    TIM14_HighHalfWord = (TIM14_HighHalfWord + 1) & 0xFFFF;
    CRITICALSECTION_FastExit();
}

uint32_t TIM14_GetCnt(void)
{
    uint32_t ret;
    CRITICALSECTION_FastEnter();
    ret = (TIM14_HighHalfWord << 16) | (TIM14->CNT & 0xFFFF);
    CRITICALSECTION_FastExit();
    return ret;
}
