/*
 * criticalsection.c
 *
 *  Created on: 2015 nov. 6
 *      Author: palotasb
 */
#include "criticalsection.h"

unsigned int CRITICALSECTION_NestingLevel = 0;
static TM CRITICALSECTION_TM;

void CRITICALSECTION_Enter(void)
{
    __disable_irq();
    CRITICALSECTION_NestingLevel++;
    if(CRITICALSECTION_NestingLevel == 1)
    {
        TMStart(&CRITICALSECTION_TM, __get_IPSR() & 0x0000003FUL);
    }
}

void CRITICALSECTION_Exit(void)
{
    if(CRITICALSECTION_NestingLevel)
    {
        CRITICALSECTION_NestingLevel--;
        if(!CRITICALSECTION_NestingLevel)
        {
            TMEnd(&CRITICALSECTION_TM);
            __enable_irq();
        }
    }
}
