/*
 * dma1_periph.h
 *
 *  Created on: 2015 nov. 7
 *      Author: palotasb
 */

#ifndef DMA1_PERIPH_H_
#define DMA1_PERIPH_H_

#include "stm32f0xx.h"
#include "debug.h"
#include "adc1_periph.h"
#include "usart1_periph.h"
#include "spi1_periph.h"

extern void (*DMA1_Channel1TCCallback)(void);
extern void (*DMA1_Channel2TCCallback)(void);
extern void (*DMA1_Channel3TCCallback)(void);

void DMA1_Init(void);
void DMA1_Channel1_ADC1_Init(void);
void DMA1_Channel2_USART1TX_Init(void);
void DMA1_Channel3_SPI1TX_Init(void);

void DMA1_Channel1_TC(void);
void DMA1_Channel2_TC(void);
void DMA1_Channel3_TC(void);

#endif /* DMA1_PERIPH_H_ */
