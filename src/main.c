//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"
#include "main.h"
#include "spi2_periph.h"

// ----------------------------------------------------------------------------
//
// Standalone STM32F0 empty sample (trace via DEBUG).
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the DEBUG output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

TM TMMain;

int main(int argc, char* argv[])
{
    volatile uint32_t x;
    IT_Init();
    //USART1_Init();
    TIM14_Init();
    COM_GPIO_InputInit();
    DEBUG_Init();
    DEBUG_SetLight(200);
    TIM16_Init();
    LTM_Init();

    LTM_MeasureBaselineThreshold();

    do
    {
        while(COM_GPIO_GetValue() == Bit_RESET)
            ;
        TMStart(&TMMain, x++);
        LTM_Start();
        TMEnd(&TMMain);
        while(COM_GPIO_GetValue() == Bit_SET)
            ;

    } while(1);

}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
