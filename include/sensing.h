/*
 * sensing.h
 *
 *  Created on: 2015 nov. 8
 *      Author: palotasb
 */

#ifndef SENSING_H_
#define SENSING_H_

#include "stm32f0xx.h"
#include "adc1_periph.h"
#include "dma1_periph.h"
#include "spi2_periph.h"
#include "criticalsection.h"
#include "debug.h"
#include "tim14_periph.h"

/** Time to wait for the analog levels to reach their final values during sensor measurement */
#define SENSING_LevelingTime    300
#define SENSING_Repeat_Infinity UINT32_MAX

typedef enum
{
    SENSING_State_Reset = 0,
    SENSING_State_Ready,
    SENSING_State_Preload,
    SENSING_State_Shifting,
    SENSING_State_WaitingForLevel,
    SENSING_State_Conversion
} SENSING_StateTypeDef;

extern SENSING_StateTypeDef SENSING_State;
extern uint32_t SENSING_MeasurementCounter;
extern uint32_t* SENSING_SensorData1;
extern uint32_t* SENSING_SensorData2;

void SENSING_Init(void);
ErrorStatus SENSING_FullMeasurement(void);
void SENSING_MaxRepeatCmd(uint32_t maxCount);

#endif /* SENSING_H_ */
