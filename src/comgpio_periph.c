/*
 * comgpio_periph.c
 *
 *  Created on: 2015 nov. 14
 *      Author: palotasb
 */

#include "comgpio_periph.h"

void COM_GPIO_OutputInit(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    COM_GPIO_RCC_BusPeriphClockCmd(COM_GPIO_RCC_BusPeriphPortENR, ENABLE);

    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_Pin = COM_GPIO_Pin;

    GPIO_Init(COM_GPIO_Port, &GPIO_InitStructure);
}

void COM_GPIO_InputInit(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    COM_GPIO_RCC_BusPeriphClockCmd(COM_GPIO_RCC_BusPeriphPortENR, ENABLE);

    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_Pin = COM_GPIO_Pin;

    GPIO_Init(COM_GPIO_Port, &GPIO_InitStructure);
}

void COM_GPIO_On(void)
{
    GPIO_SetBits(COM_GPIO_Port, COM_GPIO_Pin);
}

void COM_GPIO_Off(void)
{
    GPIO_ResetBits(COM_GPIO_Port, COM_GPIO_Pin);
}

uint8_t COM_GPIO_GetValue(void)
{
    return GPIO_ReadInputDataBit(COM_GPIO_Port, COM_GPIO_Pin);
}
