/*
 * spi1_periph.h
 *
 *  Created on: 2015 dec. 3
 *      Author: palotasb
 */

#ifndef SPI1_PERIPH_H_
#define SPI1_PERIPH_H_

#include "stm32f0xx.h"
#include "criticalsection.h"
#include "fifo.h"
#include "dma1_periph.h"

#define SPI1_Port           GPIOB
#define SPI1_RCCPeriph      RCC_AHBPeriph_GPIOB
#define SPI1_MOSI_Pin       GPIO_Pin_5
#define SPI1_MOSI_PinSource GPIO_PinSource5
#define SPI1_MISO_Pin       GPIO_Pin_4
#define SPI1_MISO_PinSource GPIO_PinSource4
#define SPI1_SCK_Pin        GPIO_Pin_3
#define SPI1_SCK_PinSource  GPIO_PinSource3

#define SPI1_NSS_Port       GPIOA
#define SPI1_NSS_Pin        GPIO_Pin_15
#define SPI1_NSS_PinSource  GPIO_PinSource15

#define SPI1_DATA_SIZE          8
#define SPI1_FRAME_FIFO_SIZE    30

typedef struct
{
    uint8_t type;
    uint8_t length;
    uint8_t data[SPI1_DATA_SIZE];
} SPI1_Frame;

#define SPI1_FRAME_SIZE         (sizeof(SPI1_Frame))

typedef enum
{
    SPI1_State_Reset = 0,
    SPI1_State_Transmitting,
    SPI1_State_Ready
} SPI1_StateTypeDef;

extern SPI1_Frame SPI1_TxDMABuff;
extern void (*SPI1_TXECallback)(void);
extern void (*SPI1_RXNECallback)(uint8_t data);

void SPI1_Init(void);
void SPI1_SendData(SPI1_Frame*);
void SPI1_TXE(void);
void SPI1_RXNE(void);

#endif /* SPI1_PERIPH_H_ */
