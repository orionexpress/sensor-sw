/*
 * debug.h
 *
 *  Created on: 2015 nov. 16
 *      Author: palotasb
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include "stm32f0xx.h"
#include "spi2_periph.h"
#include "tim3_periph.h"
#include "sensing.h"
#include "diag/Trace.h"
#include "fifo.h"

#define DEBUG_LED_ALL   0xFFFFFFFFUL
#define DEBUG_LED_B1    0x000000FFUL
#define DEBUG_LED_B2    0x0000FF00UL
#define DEBUG_LED_B3    0x00FF0000UL
#define DEBUG_LED_B4    0xFF000000UL
#define DEBUG_LED_HW1   0x0000FFFFUL
#define DEBUG_LED_HW2   0xFFFF0000UL

extern uint32_t DEBUG_Leds;

void DEBUG_SetLeds(uint32_t);
void DEBUG_ResetLeds(uint32_t);
void DEBUG_ToggleLeds(uint32_t);
void DEBUG_Update(void);
void DEBUG_Init(void);
void DEBUG_SetLight(uint8_t);
void DEBUG_Write(char*);
void DEBUG_WriteU8(uint8_t);
void DEBUG_WriteU8D(uint8_t);
void DEBUG_WriteU16(uint16_t);
void DEBUG_WriteU16D(uint16_t);
void DEBUG_WriteU32(uint32_t);

#endif /* DEBUG_H_ */
