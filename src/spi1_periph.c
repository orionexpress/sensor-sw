/*
 * spi1_periph.c
 *
 *  Created on: 2015 dec. 3
 *      Author: palotasb
 */

#include "spi1_periph.h"

#define SPI1_NO_DMA

void (*SPI1_RXNECallback)(uint8_t data) = ((void (*)(uint8_t)) 0);

static FIFO TXFIFO;
static SPI1_Frame TXFIFOData[SPI1_FRAME_FIFO_SIZE];
static SPI1_StateTypeDef SPI1_State;

SPI1_Frame SPI1_TxDMABuff;

static void SPI1_Transmit(SPI1_Frame*);

void SPI1_Init(void)
{
    /* - Init SPI peripheral.
     * - Init interrupts.
     * - Init internal variables.
     */
    SPI_InitTypeDef SPI1_InitStructure;
    GPIO_InitTypeDef SPI1_GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

    GPIO_PinAFConfig(SPI1_Port, SPI1_SCK_PinSource, GPIO_AF_0);
    GPIO_PinAFConfig(SPI1_Port, SPI1_MOSI_PinSource, GPIO_AF_0);
    GPIO_PinAFConfig(SPI1_Port, SPI1_MISO_PinSource, GPIO_AF_0);
    //GPIO_PinAFConfig(SPI1_NSS_Port, SPI1_NSS_PinSource, GPIO_AF_0);

    SPI1_GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    SPI1_GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    SPI1_GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    SPI1_GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;
    SPI1_GPIO_InitStructure.GPIO_Pin = SPI1_SCK_Pin | SPI1_MOSI_Pin | SPI1_MISO_Pin;
    GPIO_Init(SPI1_Port, &SPI1_GPIO_InitStructure);

    GPIO_SetBits(SPI1_NSS_Port, SPI1_NSS_Pin);
    SPI1_GPIO_InitStructure.GPIO_Pin = SPI1_NSS_Pin;
    SPI1_GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_Init(SPI1_NSS_Port, &SPI1_GPIO_InitStructure);

    SPI1_InitStructure.SPI_Direction = SPI_Direction_1Line_Tx;
    SPI1_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI1_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI1_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
    SPI1_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI1_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI1_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128;
    SPI1_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI1_InitStructure.SPI_CRCPolynomial = 1;

    SPI_Init(SPI1, &SPI1_InitStructure);

    TXFIFO.itemSize = sizeof(SPI1_Frame);
    TXFIFO.length = SPI1_FRAME_FIFO_SIZE;
    TXFIFO.items = (void*) TXFIFOData;
    FIFO_Init(&TXFIFO);

#if !defined(SPI1_NO_DMA)
    DMA1_Channel3TCCallback = &SPI1_TXE;

    DMA1_Channel3_SPI1TX_Init();
    SPI_I2S_DMACmd(SPI1, SPI_I2S_DMAReq_Tx, ENABLE);
#endif
    SPI_Cmd(SPI1, ENABLE);

    SPI1_State = SPI1_State_Ready;
}

void SPI1_TXE(void)
{
    SPI1_Frame frame;
    if(TXFIFO.cnt)
    {
        FIFO_Pop(&TXFIFO, &frame);
        SPI1_Transmit(&frame);
        SPI1_State = SPI1_State_Transmitting;
    }
    else
    {
        SPI1_State = SPI1_State_Ready;
        DMA_ITConfig(DMA1_Channel3, DMA_IT_TC, DISABLE);
    }
}

void SPI1_RXNE(void)
{
    volatile __attribute((unused))uint8_t tmp = SPI1->DR;
}

static void SPI1_Transmit(SPI1_Frame * frame)
{
    SPI1_TxDMABuff = *frame;
    DMA_SetCurrDataCounter(DMA1_Channel3, SPI1_FRAME_SIZE);
    DMA_ClearFlag (DMA1_FLAG_TC3);
    DMA_ITConfig(DMA1_Channel3, DMA_IT_TC, ENABLE);
    DMA_Cmd(DMA1_Channel3, ENABLE);
}

void SPI1_SendData(SPI1_Frame * frame)
{
#if !defined(SPI1_NO_DMA)
    CRITICALSECTION_Enter();
    if(SPI1_State == SPI1_State_Ready)
    {
        SPI1_State = SPI1_State_Transmitting;
        // *frame gets copied into the DMA buffer and gets sent out
        SPI1_Transmit(frame);
    }
    else if(SPI1_State == SPI1_State_Transmitting)
    {
        // *frame gets copied into the FIFO buffer and gets sent out later
        FIFO_Add(&TXFIFO, (void*) frame);
        DMA_ITConfig(DMA1_Channel3, DMA_IT_TC, ENABLE);
    }
    CRITICALSECTION_Exit();
#else
    uint32_t i;
    uint8_t* frptr = (uint8_t*) frame;
    volatile uint32_t j;
    GPIO_ResetBits(SPI1_NSS_Port, SPI1_NSS_Pin);
    for(i = 0; i < SPI1_FRAME_SIZE; i++)
    {
        while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET)
            ;
        __DMB();
        SPI_SendData8(SPI1, frptr[i]);
        __DMB();
    }
    while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET)
        ;
    j = 100;
    for(; j; --j)
        ;
    __DMB();
    GPIO_SetBits(SPI1_NSS_Port, SPI1_NSS_Pin);
#endif
}
