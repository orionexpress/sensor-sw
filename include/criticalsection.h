/*
 * criticalsection.h
 *
 *  Created on: 2015 nov. 6
 *      Author: palotasb
 */

#ifndef CRITICALSECTION_H_
#define CRITICALSECTION_H_

#include "stm32f0xx.h"
#include "util.h"

extern unsigned int CRITICALSECTION_NestingLevel;

void CRITICALSECTION_Enter(void);
void CRITICALSECTION_Exit(void);
static inline void CRITICALSECTION_FastEnter(void);
static inline void CRITICALSECTION_FastExit(void);

static inline void CRITICALSECTION_FastEnter(void)
{
    __disable_irq();
    CRITICALSECTION_NestingLevel++;
}

static inline void CRITICALSECTION_FastExit(void)
{
    if(CRITICALSECTION_NestingLevel)
    {
        CRITICALSECTION_NestingLevel--;
        if(!CRITICALSECTION_NestingLevel)
        {
            __enable_irq();
        }
    }
}

#endif /* CRITICALSECTION_H_ */
