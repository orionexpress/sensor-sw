/*
 * linetrack_modules.h
 *
 *  Created on: 2015 nov. 16
 *      Author: palotasb
 */

#ifndef LINETRACK_MODULES_H_
#define LINETRACK_MODULES_H_

#include "linetrack.h"
#include "sensing.h"
#include "debug.h"
#include "spi2_periph.h"
#include "serialmessaging.h"

#define LTM1_SENSORCOUNT    32
#define LTM2_SENSORCOUNT    32
#define LTM_LINECOUNT       8

extern LINETRACK_ModuleTypeDef LTM1;
extern uint32_t LTM1_SensorData[LTM1_SENSORCOUNT];
extern uint32_t LTM1_TrackingPositions[LTM_LINECOUNT];
extern uint32_t LTM1_FilteredTrackingPositions[LTM_LINECOUNT];
extern uint32_t LTM1_SensorDataTmp[LTM1_SENSORCOUNT];
extern uint8_t LTM1_SensorDataAboveDetectionThreshold[LTM1_SENSORCOUNT];

extern LINETRACK_ModuleTypeDef LTM2;
extern uint32_t LTM2_SensorData[LTM2_SENSORCOUNT];
extern uint32_t LTM2_TrackingPositions[LTM_LINECOUNT];
extern uint32_t LTM2_FilteredTrackingPositions[LTM_LINECOUNT];
extern uint32_t LTM2_SensorDataTmp[LTM2_SENSORCOUNT];
extern uint8_t LTM2_SensorDataAboveDetectionThreshold[LTM2_SENSORCOUNT];

void LTM_Init(void);
void LTM_MeasureBaselineThreshold(void);
void LTM_Calculate(void);
void LTM_Start(void);

#endif /* LINETRACK_MODULES_H_ */
