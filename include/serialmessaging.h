/*
 * serialmessaging.h
 *
 *  Created on: 2015 dec. 8
 *      Author: palotasb
 */

#ifndef SERIALMESSAGING_H_
#define SERIALMESSAGING_H_

#include "stm32f0xx.h"
#include "criticalsection.h"
#include "spi1_periph.h"

typedef enum
{
    SM_State_Reset = 0,
    SM_State_Ready,
    SM_State_Receiving
} SM_StateTypeDef;

extern SM_StateTypeDef SM_State;

void SM_Init(void);
void SM_Receive(uint8_t data);
void SM_Register(uint8_t type, uint8_t* buffer, void (*fn)(uint8_t, uint8_t, uint8_t*));
void SM_Unregister(uint8_t type);
void SM_Send(uint8_t type, uint8_t length, uint8_t* data);

#endif /* SERIALMESSAGING_H_ */
