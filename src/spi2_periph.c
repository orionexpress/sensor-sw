/*
 * spi2_periph.c
 *
 *  Created on: 2015 nov. 6
 *      Author: palotasb
 */

#include "spi2_periph.h"

SPI2_StateTypeDef SPI2_State;

#define SPI2_OUTPUTDATA_CNT32                   3
#define SPI2_OUTPUTDATA_CNT16                   6
#define SPI2_OUTDATA32_DEBUGLED_POS             2
#define SPI2_OUTDATA32_LINE1_POS                0
#define SPI2_OUTDATA32_LINE2_POS                1
#define SPI2_OUTDATA16_LATCH_SENS               5
#define SPI2_OUTDATA16_LATCH_DEBUGLED           1

uint32_t SPI2_OutputData32[SPI2_OUTPUTDATA_CNT32];

uint16_t * const SPI2_OutputData16 = (uint16_t *) SPI2_OutputData32;
uint32_t * const SPI2_SensOutput = &SPI2_OutputData32[SPI2_OUTDATA32_LINE1_POS];
uint32_t * const SPI2_DebugLEDOutput = &SPI2_OutputData32[SPI2_OUTDATA32_DEBUGLED_POS];

FlagStatus SPI2_DataDirtyFlag = RESET;
FlagStatus SPI2_Shift5Flag = RESET;

inline static void SPI2_ConfigFull(void);
inline static void SPI2_ConfigShift(void);
static void SPI2_SensLatchCmd(void);
static void SPI2_DebugLEDLatchCmd(void);
static void SPI2_SendData(void);
static void SPI2_WaitForTXE(void);

void SPI2_Init(void)
{
    /* - Init internal FIFOs.
     * - Init SPI peripheral.
     * - Init interrupts.
     * - Init internal variables.
     */
    SPI_InitTypeDef SPI2_InitStructure;
    GPIO_InitTypeDef SPI2_GPIO_InitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

    SPI2_GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    SPI2_GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    SPI2_GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    SPI2_GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;
    SPI2_GPIO_InitStructure.GPIO_Pin = SPI2_SCK_Pin | SPI2_MOSI_Pin;

    GPIO_Init(GPIOB, &SPI2_GPIO_InitStructure);
    GPIO_PinAFConfig(SPI2_SCK_Port, SPI2_SCK_PinSource, GPIO_AF_0);
    GPIO_PinAFConfig(SPI2_MOSI_Port, SPI2_MOSI_PinSource, GPIO_AF_0);

    SPI2_GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    SPI2_GPIO_InitStructure.GPIO_Pin = SPI2_SENS_OE_Pin | SPI2_LED_LE_Pin | SPI2_SENS_LE_Pin;
    GPIO_Init(SPI2_COMMON_Port, &SPI2_GPIO_InitStructure);

    SPI2_InitStructure.SPI_Direction = SPI_Direction_1Line_Tx;
    SPI2_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI2_InitStructure.SPI_DataSize = SPI_DataSize_16b;
    SPI2_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
    SPI2_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI2_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI2_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;
    SPI2_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI2_InitStructure.SPI_CRCPolynomial = 1;

    SPI_Init(SPI2, &SPI2_InitStructure);

    SPI2_DataDirtyFlag = RESET;
    SPI2_Shift5Flag = RESET;

    SPI_Cmd(SPI2, ENABLE);

    SPI2_State = SPI2_State_Ready;
}

static void SPI2_SendData(void)
{
    uint32_t SPI2_OutputDataCounter16;

    CRITICALSECTION_Enter();
    {
        if(SPI2_State == SPI2_State_Ready)
        {
            SPI2_State = SPI2_State_Waiting;
            CRITICALSECTION_Exit();
        }
        else
        {
            CRITICALSECTION_Exit();
            return;
        }
    }

    if(SPI2_Shift5Flag == SET)
    {
        SPI2_ConfigShift();

        GPIO_ResetBits(SPI2_SENS_LE_Port, SPI2_SENS_LE_Pin);
        GPIO_ResetBits(SPI2_LED_LE_Port, SPI2_LED_LE_Pin);

        SPI2_WaitForTXE();
        SPI_SendData8(SPI2, (uint8_t) SPI2_OutputData16[0]);
        SPI2_WaitWhileBusy();
        SPI2_SensLatchCmd();

        SPI2_Shift5Flag = RESET;
    }
    else if(SPI2_DataDirtyFlag == SET)
    {
        SPI2_ConfigFull();
        for(SPI2_OutputDataCounter16 = 0; SPI2_OutputDataCounter16 < SPI2_OUTPUTDATA_CNT16; SPI2_OutputDataCounter16++)
        {
            SPI2_WaitForTXE();
            SPI_I2S_SendData16(SPI2, SPI2_OutputData16[SPI2_OUTPUTDATA_CNT16 - SPI2_OutputDataCounter16 - 1]);

            switch(SPI2_OutputDataCounter16)
            {
                case SPI2_OUTDATA16_LATCH_DEBUGLED:
                    SPI2_WaitWhileBusy();
                    SPI2_DebugLEDLatchCmd();
                break;
                case SPI2_OUTDATA16_LATCH_SENS:
                    SPI2_WaitWhileBusy();
                    SPI2_SensLatchCmd();
                break;
            }
        }

        SPI2_DataDirtyFlag = RESET;
    }

    SPI2_State = SPI2_State_Ready;
}

void SPI2_SendDebugLedData(uint32_t line1)
{
    CRITICALSECTION_Enter();
    {
        SPI2_DebugLEDOutput[0] = line1;
        SPI2_DataDirtyFlag = SET;
    }
    CRITICALSECTION_Exit();

    SPI2_SendData();
}

void SPI2_SendSensPreloadData(uint32_t line1, uint32_t line2)
{
    CRITICALSECTION_Enter();
    {
        SPI2_SensOutput[1] = line2;
        SPI2_SensOutput[0] = line1;
        SPI2_DataDirtyFlag = SET;
        SPI2_Shift5Flag = RESET;
    }
    CRITICALSECTION_Exit();

    SPI2_SendData();
}

void SPI2_ShiftSensData(uint8_t data)
{
    data &= SPI2_SHIFT_MASK;
    CRITICALSECTION_Enter();
    {
        // Line 2 (MSW)
        SPI2_SensOutput[1] = (SPI2_SensOutput[1] << SPI2_SHIFT_BITS) | (SPI2_SensOutput[0] >> (32 - SPI2_SHIFT_BITS));
        // Line 1 (LSW)
        SPI2_SensOutput[0] = (SPI2_SensOutput[0] << SPI2_SHIFT_BITS) | (data);

        if(SPI2_Shift5Flag == RESET && SPI2_DataDirtyFlag == RESET)
        {
            SPI2_Shift5Flag = SET;
        }
        else
        {
            SPI2_Shift5Flag = RESET;
            SPI2_DataDirtyFlag = SET;
        }
    }
    CRITICALSECTION_Exit();

    SPI2_SendData();
}

void SPI2_SensOutputCmd(FunctionalState newState)
{
    if(newState != DISABLE)
    {
        GPIO_ResetBits(SPI2_SENS_OE_Port, SPI2_SENS_OE_Pin);
    }
    else
    {
        GPIO_SetBits(SPI2_SENS_OE_Port, SPI2_SENS_OE_Pin);
    }
}

void SPI2_LEDOutputCmd(FunctionalState newState)
{
    if(newState != DISABLE)
    {
        GPIO_ResetBits(SPI2_LED_OE_Port, SPI2_LED_OE_Pin);
    }
    else
    {
        GPIO_SetBits(SPI2_LED_OE_Port, SPI2_LED_OE_Pin);
    }
}

static inline void SPI2_ConfigFull(void)
{
    SPI_DataSizeConfig(SPI2, SPI_DataSize_16b);
}

static inline void SPI2_ConfigShift(void)
{
    SPI_DataSizeConfig(SPI2, SPI_DataSize_5b);
}

static void SPI2_SensLatchCmd(void)
{
    volatile uint32_t to = SPI2_LATCH_TO;
    GPIO_SetBits(SPI2_SENS_LE_Port, SPI2_SENS_LE_Pin);
    while(to-- > 0)
        ;
    GPIO_ResetBits(SPI2_SENS_LE_Port, SPI2_SENS_LE_Pin);
}

static void SPI2_DebugLEDLatchCmd(void)
{
    volatile uint32_t to = SPI2_LATCH_TO;
    GPIO_SetBits(SPI2_LED_LE_Port, SPI2_LED_LE_Pin);
    while(to-- > 0)
        ;
    GPIO_ResetBits(SPI2_LED_LE_Port, SPI2_LED_LE_Pin);
}

static void SPI2_WaitForTXE(void)
{
    while(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET)
        ;
}
