/*
 * fixedpoint.h
 *
 *  Created on: 2015 dec. 5
 *      Author: palotasb
 */

#ifndef FIXEDPOINT_H_
#define FIXEDPOINT_H_

typedef uint32_t uf32p0;
typedef uint32_t uf24p8;
typedef uint32_t uf16p16;
typedef uint32_t uf8p24;
typedef uint32_t uf0p32;

typedef int32_t f32p0;
typedef int32_t f24p8;
typedef int32_t f16p16;
typedef int32_t f8p24;
typedef int32_t f0p32;

#endif /* FIXEDPOINT_H_ */
