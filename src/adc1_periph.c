/*
 * adc1_periph.c
 *
 *  Created on: 2015 nov. 7
 *      Author: palotasb
 */

#include "adc1_periph.h"

uint32_t ADC1_Data[8];

void ADC1_Init(void)
{
    ADC_InitTypeDef ADC1_InitStructure;
    GPIO_InitTypeDef GPIOA_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

    GPIOA_InitStructure.GPIO_Mode = GPIO_Mode_AN;
    GPIOA_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIOA_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIOA_InitStructure.GPIO_Speed = GPIO_Speed_Level_3;
    GPIOA_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5
            | GPIO_Pin_6 | GPIO_Pin_7;

    GPIO_Init(GPIOA, &GPIOA_InitStructure);

    ADC1_InitStructure.ADC_Resolution = ADC_Resolution_8b;
    ADC1_InitStructure.ADC_ContinuousConvMode = DISABLE;
    ADC1_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
    ADC1_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_TRGO;
    ADC1_InitStructure.ADC_DataAlign = ADC_DataAlign_Left;
    ADC1_InitStructure.ADC_ScanDirection = ADC_ScanDirection_Upward;

    ADC_Init(ADC1, &ADC1_InitStructure);

    ADC_Cmd(ADC1, ENABLE);

    ADC_ChannelConfig(ADC1,
                      ADC_Channel_0 | ADC_Channel_1 | ADC_Channel_2 | ADC_Channel_3 | ADC_Channel_4 | ADC_Channel_5
                              | ADC_Channel_6 | ADC_Channel_7,
                      ADC_SampleTime_7_5Cycles);

    ADC_DiscModeCmd(ADC1, DISABLE);
    ADC_ContinuousModeCmd(ADC1, DISABLE);
    ADC_OverrunModeCmd(ADC1, ENABLE);

    DMA1_Channel1_ADC1_Init();
    ADC_DMACmd(ADC1, ENABLE);
    ADC_DMARequestModeConfig(ADC1, ADC_DMAMode_Circular);
}

void ADC1_StartConversion(void)
{
    ADC_StartOfConversion (ADC1);
}
