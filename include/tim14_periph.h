/*
 * tim14_periph.h
 *
 *  Created on: 2015 dec. 12
 *      Author: palotasb
 */

#ifndef TIM14_PERIPH_H_
#define TIM14_PERIPH_H_

#include "stm32f0xx.h"
#include "criticalsection.h"

void TIM14_Init(void);
void TIM14_UE(void);
uint32_t TIM14_GetCnt(void);

extern uint32_t TIM14_HighHalfWord;

#endif /* TIM14_PERIPH_H_ */
